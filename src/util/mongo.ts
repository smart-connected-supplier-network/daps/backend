import { MongoClient } from 'mongodb';
import promiseRetry from 'promise-retry';
import { Configuration } from './configuration';

const configuration = Configuration.getInstance();

/** Connect to a MongoDB instance with provided environment variables in an retryable fashion */
export async function connectMongoDB (): Promise<MongoClient> {
  const url = `mongodb://${configuration.mongo.username}:${configuration.mongo.password}@${configuration.mongo.hostname}:${configuration.mongo.port}/?tls=${configuration.mongo.sslEnabled}`;
  return await promiseRetry(async (retry, attempt) => {
    try {
      return await MongoClient.connect(url);
    } catch (err) {
      console.error('[MongoDB] Cannot connect to MongoDB - Retrying');
      if (err instanceof Error && attempt > 5) {
        console.error(err.message);
      }
      return retry(err);
    }
  });
}
