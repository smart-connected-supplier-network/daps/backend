'use strict';
import * as jwt from 'jsonwebtoken';
import { ComponentStore } from '../stores/component-store';
import { Request, Response } from 'express';
import { Configuration } from '../util/configuration';
import { DynamicClaimsRequest } from '../model';

const configuration = Configuration.getInstance();
const componentStore = ComponentStore.getInstance();

/** DAPS Token handler */
export async function handlev2 (req: Request, res: Response): Promise<void> {
  try {
    const [assertion, scope] =
      req.body.grant_type === 'client_credentials'
        ? [req.body.client_assertion, req.body.scope]
        : [req.body.assertion, 'idsc:IDS_CONNECTOR_ATTRIBUTES_ALL'];
    const dynamicClaimsRequest: DynamicClaimsRequest | undefined = req.body.claims;
    const assertionDecoded = jwt.decode(assertion, { json: true });
    if (!assertionDecoded) {
      console.log(`[TokenHandler] Could not parse incoming assertion: ${assertion}`);
      res.status(400).send({
        status: 'Error',
        message: 'Could not parse incoming assertion'
      });
      return;
    }
    const dapsEntry = await componentStore.get(assertionDecoded.iss);
    if (!dapsEntry) {
      console.log(`[TokenHandler] Did not find the certificate for subject: ${assertionDecoded.iss}`);
      res.status(404).send({
        status: 'NotFound',
        message: `Did not find the certificate for subject: ${assertionDecoded.iss}`
      });
      return;
    }
    console.log(
      `[TokenHandler] Verifying JWT against public key: ${assertionDecoded.iss} (cert length: ${dapsEntry.certificate.length})`
    );
    try {
      jwt.verify(assertion, dapsEntry.certificate);
    } catch (e) {
      if (configuration.strict) {
        console.log(
          `[TokenHandler] Verification failed, not creating DAT: ${e.message}`
        );
        res.status(400).send({
          status: 'Error',
          reason: `Verification of client assertion failed: ${e.message}`
        });
        return;
      } else {
        console.log(
          `[TokenHandler] Verification failed, ignoring: ${e.message}`
        );
      }
    }
    const signOptions: jwt.SignOptions = {
      issuer: configuration.issuer,
      subject: assertionDecoded.sub,
      audience: 'idsc:IDS_CONNECTORS_ALL',
      expiresIn: '1h',
      algorithm: 'RS256',
      keyid: 'default',
      notBefore: 0
    };
    let securityProfile;
    switch (dapsEntry.claim.componentCertification.securityProfile) {
      case 'Base':
        securityProfile = 'idsc:BASE_SECURITY_PROFILE';
        break;
      case 'Trust':
        securityProfile = 'idsc:TRUST_SECURITY_PROFILE';
        break;
      case 'Trust+':
        securityProfile = 'idsc:TRUST_PLUS_SECURITY_PROFILE';
        break;
    }
    const datClaims: {
      [property: string]: any
    } = {
      '@context': 'https://w3id.org/idsa/contexts/context.jsonld',
      '@type': 'ids:DatPayload',
      scopes: [scope],
      referringConnector: dapsEntry.idsid,
      securityProfile: securityProfile
    };
    if (dynamicClaimsRequest?.access_token) {
      Object.entries(dynamicClaimsRequest?.access_token).forEach(([claim, value]) => {
        datClaims[claim] = value.value;
      });
    }
    const token = jwt.sign(
      datClaims,
      configuration.key,
      signOptions
    );
    res.send({
      access_token: token,
      token_type: 'Bearer'
    });
  } catch (e) {
    console.log('[TokenHandler] Unexpected error in handling token request');
    console.log(e);
    res.status(500).send({
      status: 'Error',
      reason: `Unexpected error: ${e.message}`
    });
  }
}
