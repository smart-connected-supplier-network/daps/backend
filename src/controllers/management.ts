import express, { Router } from 'express';
import passport from 'passport';
import crypto from 'crypto';
import { Configuration } from '../util/configuration';
import { UserStore } from '../stores/user-store';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { router as loginRouter } from '../routers/login-management';
import { router as certRouter } from '../routers/certificate-management';
import { router as userRouter } from '../routers/user-management';
import { router as federatedDapsRouter } from '../routers/federated-dapsmanagement';

/** Main router for management purposes, actual routing is done in sub-routers */
export const router: Router = express.Router();

const configuration = Configuration.getInstance();
const userStore = UserStore.getInstance();

/** JWT secret for signing user tokens */
export const jwtSecret = crypto.randomBytes(32).toString('hex');

/** Passport strategy for Bearer token that will be parsed into User objects */
passport.use(
  new Strategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: jwtSecret,
      issuer: configuration.issuer,
      audience: configuration.issuer
    },
    function (jwtPayload, done) {
      userStore.getUser(jwtPayload.sub).then(
        (user) => {
          done(null, user);
        },
        (reason) => {
          done(reason, null);
        }
      );
    }
  )
);

/** Sub routers registration */
router.use(loginRouter);
router.use(userRouter);
router.use(certRouter);
router.use(federatedDapsRouter);
