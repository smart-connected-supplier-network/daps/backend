'use strict';

import express, { Application, Request, Response } from 'express';
import { Server } from 'http';
import * as https from 'https';
import * as bodyParser from 'body-parser';

import { JWK, JWKS } from 'jose';

import { ComponentStore } from './stores/component-store';
import * as tokenHandler from './controllers/token-handler';
import * as management from './controllers/management';
import { ParticipantStore } from './stores/participant-store';
import { Configuration } from './util/configuration';
import { FederatedDapsStore } from './stores/federated-daps-store';

const app: Application = express();

/** Configuration */
const configuration = Configuration.getInstance();

const componentStore: ComponentStore = ComponentStore.getInstance();
const participantStore: ParticipantStore = ParticipantStore.getInstance();

/** Allow URL encoded request parsing, for /token endpoints */
app.use(bodyParser.urlencoded({ extended: false }));
/** Allow JSON requests body parsing, for CSR handling and Admin pages */
app.use(bodyParser.json());
/** Use Management Express router */
app.use('/management', management.router);

/** Handle Cross-Origin Resource Sharing (CORS) */
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  // intercept OPTIONS method
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

/** Create Express Server */
let server: Server;
if (configuration.useHttps) {
  server = https
    .createServer(
      {
        key: configuration.key,
        cert: configuration.cert,
        ca: configuration.CAChain
      },
      app
    )
    .listen(8080, function () {
      console.log('[Main] Daps started on 0.0.0.0:8080 (https) ');
    });
} else {
  server = app.listen(8080, function () {
    console.log('[Main] Daps started on 0.0.0.0:8080 (http) ');
  });
}
server.on('close', () => {
  console.log('Shutting down server');
});

/** Token request handler v2 */
app.post('(/v2)?/token', tokenHandler.handlev2);

/** Certificate list API */
app.get('/certs', async (_, res) => {
  const [components, participants] = await Promise.all([
    componentStore.listPublic(),
    participantStore.listPublic()
  ]);
  res.send({ components: components, participants: participants });
});

/** Publish public key (in JSON Web Key Set format) */
const jwks = new JWKS.KeyStore([
  JWK.asKey(configuration.key, { kid: 'default' })
]).toJWKS(false);

const federatedDapsStore = configuration.dapsFederation
  ? FederatedDapsStore.getInstance()
  : undefined;

app.get('(/v2)?/.well-known/jwks.json', async (req: Request, res: Response) => {
  if (req.query.issuer && req.query.issuer !== configuration.issuer) {
    res.send((await federatedDapsStore?.getDapsEntry(req.query.issuer as string))?.jwks || jwks);
  } else {
    res.send(jwks);
  }
});
