'use strict';
import { load } from 'js-yaml';
import MongoClient, { DeleteResult, Document, UpdateResult } from 'mongodb';
import { ComponentStore } from './component-store';
import { ParticipantInternal, Participant } from '../model';
import { connectMongoDB } from '../util/mongo';
import { Configuration } from '../util/configuration';

const configuration = Configuration.getInstance();

/** Participant Store for persisting component entries */
export class ParticipantStore {
  /** MongoDB participant collection name */
  private readonly dbName = 'participants';

  /** Singleton instance */
  private static instance: ParticipantStore;

  /** MongoDB participant collection */
  private collection: MongoClient.Collection<ParticipantInternal>;

  /** Get or create singleton instance */
  static getInstance (): ParticipantStore {
    if (!ParticipantStore.instance) {
      ParticipantStore.instance = new ParticipantStore();
      ParticipantStore.instance.init();
    }
    return ParticipantStore.instance;
  }

  /** Initialize ParticipantStore */
  async init (): Promise<void> {
    try {
      const client = await connectMongoDB();
      this.collection = client.db(this.dbName).collection('participants');
      const count = await this.collection.countDocuments();

      if (count > 0) {
        console.log(
          `[ParticipantStore] Collection not empty, not initializing with ${Configuration.fileLocation(
            'INITIAL_PARTICIPANTS'
          )}`
        );
        return;
      }
      if (configuration.initial.participants === undefined) {
        console.log(
          `[ParticipantStore] No initial components to load, ${Configuration.fileLocation(
            'INITIAL_PARTICIPANTS'
          )} does not exist`
        );
        return;
      }
      await this.insertInitialState();
    } catch (err) {
      console.log(
        `[ParticipantStore] Error in requesting document count: ${err}`
      );
    }
  }

  /** Insert initial state from /app/conf/participants.yaml into the MongoDB collection */
  private async insertInitialState (): Promise<void> {
    const initialKeys: ParticipantInternal[] = load(
      configuration.initial.participants
    ) as ParticipantInternal[];
    initialKeys.forEach((entry) => {
      entry.owner = 'admin';
    });
    console.log(
      '[ParticipantStore] Inserting initial participant state to database'
    );
    await this.collection.insertMany(initialKeys);
  }

  /** Retrieve participants for internal use */
  public async list (): Promise<ParticipantInternal[]> {
    return this.collection?.find({})?.toArray() || [];
  }

  /** Retrieve participants for public use  */
  public async listPublic (): Promise<Participant[]> {
    const list = await this.list();
    return list.map((e) => {
      return {
        id: e.id,
        contact: e.contact,
        certificate: e.certificate,
        certification: e.certification,
        owner: e.owner
      };
    });
  }

  /** Retrieve a specific participant */
  public async get (id: string): Promise<ParticipantInternal> {
    return this.collection?.findOne({ id: id });
  }

  /** Replace a participant with a given id */
  public replace (
    id: string,
    participant: ParticipantInternal
  ): Promise<UpdateResult | Document> {
    return this.collection?.replaceOne({ id: id }, participant, {
      upsert: true
    });
  }

  /** Delete a participant with a given id */
  public delete (id: string): Promise<DeleteResult> {
    return this.collection?.deleteOne({ id: id });
  }

  /** Update the certification for a participant with a given id */
  public async certifyParticipant (
    id: string,
    certify: boolean,
    level: string
  ): Promise<UpdateResult | Document> {
    const now = new Date();
    await this.collection?.updateOne(
      { id: id },
      {
        $set: {
          'certification.active': certify,
          'certification.level': level,
          'certification.lastActive': now
        }
      }
    );
    return ComponentStore.getInstance().certifyParticipant(
      id,
      certify,
      level,
      now
    );
  }
}
