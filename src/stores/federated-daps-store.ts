'use strict';

import MongoClient from 'mongodb';
import promiseRetry from 'promise-retry';
import { load } from 'js-yaml';
import { connectMongoDB } from '../util/mongo';
import { Configuration } from '../util/configuration';
import { FederatedDAPSEntry } from '../model';
import axios from 'axios';
import { JSONWebKeySet } from 'jose';

const configuration = Configuration.getInstance();

/** Federated DAPS Store for persisting federated DAPS instances */
export class FederatedDapsStore {
  /** Singleton instance */
  private static instance: FederatedDapsStore;
  /** MongoDB database name */
  private readonly dbName = 'daps';
  /** MongoDB collection */
  private collection: MongoClient.Collection<FederatedDAPSEntry>;
  private interval: NodeJS.Timeout;

  /** Get or create singleton instance */
  static getInstance (): FederatedDapsStore {
    if (!FederatedDapsStore.instance) {
      FederatedDapsStore.instance = new FederatedDapsStore();
      FederatedDapsStore.instance.init();
    }
    return FederatedDapsStore.instance;
  }

  /** Initialize Federated DAPS store */
  async init (): Promise<void> {
    try {
      const client = await connectMongoDB();
      this.collection = client.db(this.dbName).collection('federated');
      const count = await this.collection.countDocuments();
      if (count > 0) {
        console.log(
          `[FederatedDapsStore] Collection not empty, not initializing with ${Configuration.fileLocation(
            'INITIAL_FEDERATED_DAPS'
          )}`
        );
        return;
      }
      if (configuration.initial.federatedDaps === undefined) {
        console.log(
          `[FederatedDapsStore] No initial federated DAPS instances to load, ${Configuration.fileLocation(
            'INITIAL_FEDERATED_DAPS'
          )} does not exist`
        );
        return;
      }
      await this.insertInitialState();
      this.interval = setInterval(
        this.refreshJwks,
        configuration.dapsFederationInterval
      );
    } catch (err) {
      console.log(`[FederatedDapsStore] Error in mongodb connection: ${err}`);
    }
  }

  async refreshJwks (): Promise<void> {
    const entries = await this.getAllDapsEntries();
    await Promise.all(
      entries.map(async (entry) => {
        try {
          const jwks = await this.fetchJWKS(entry.url, 1);
          entry.jwks = jwks;
          console.log(
            '[FederatedDapsStore] Inserting initial federated daps to database'
          );
          await this.collection.replaceOne({ dapsid: entry.dapsid }, entry);
        } catch (err) {
          console.error(
            `[FederatedDapsStore] Error updating JWKS for ${entry.dapsid}`
          );
        }
      })
    );
  }

  /** Insert initial state of Federated DAPS entries into the MongoDB collection */
  private async insertInitialState (): Promise<void> {
    const initialKeys: FederatedDAPSEntry[] = load(
      configuration.initial.federatedDaps
    ) as FederatedDAPSEntry[];
    if (initialKeys.length > 0) {
      await Promise.all(
        initialKeys.map(async (entry) => {
          try {
            const jwks = await this.fetchJWKS(entry.url);
            entry.jwks = jwks;
          } catch (err) {
            console.log(
              `[FederatedDapsStore] Unable to refresh JWKS for ${entry.dapsid}`
            );
          }
          console.log(
            '[FederatedDapsStore] Inserting initial federated daps to database'
          );
          await this.collection.insertOne(entry);
        })
      );
    }
  }

  /** Fetch JSON Web Key Set from remote DAPS */
  private fetchJWKS (url: string, retries = 3): Promise<JSONWebKeySet> {
    return promiseRetry({ retries: retries }, async (retry) => {
      try {
        const response = await axios.get(url);
        const jwks: JSONWebKeySet = await response.data;

        if (jwks.keys?.length > 0) {
          return jwks;
        }

        throw new Error(
          '[FederatedDapsStore] Received incorrect JWKS response'
        );
      } catch (err) {
        console.error(
          `[FederatedDapsStore] Cannot retrieve jwks from ${url} - Retrying`
        );
        return retry(err);
      }
    });
  }

  /** Insert new DAPS entry,  */
  public async addDapsEntry (
    dapsid: string,
    url: string
  ): Promise<FederatedDAPSEntry> {
    const foundDaps = await this.getDapsEntry(dapsid);
    if (!foundDaps) {
      try {
        const jwks = await this.fetchJWKS(url);
        const federatedDaps: FederatedDAPSEntry = {
          dapsid,
          url,
          jwks,
          trusted: true
        };
        await this.collection?.insertOne(federatedDaps);
        return federatedDaps;
      } catch (err) {
        throw new Error(
          '[FederatedDapsStore] Failed to retrieve jwks , did not add DAPS'
        );
      }
    } else {
      throw new Error(
        '[FederatedDapsStore] Found dapsid in database, unable to add DAPS with id ' +
          dapsid
      );
    }
  }

  /** Get all federated DAPS entries */
  public async getAllDapsEntries (): Promise<FederatedDAPSEntry[]> {
    return this.collection?.find({})?.toArray() || [];
  }

  /** Get DAPS entry for given identifier */
  public async getDapsEntry (
    dapsid: string
  ): Promise<FederatedDAPSEntry | null> {
    return this.collection?.findOne({ dapsid: dapsid });
  }

  /** Delete DAPS entry for given identifier */
  public async deleteDapsEntry (dapsid: string): Promise<boolean> {
    return (
      (await this.collection?.deleteOne({ dapsid: dapsid }))?.deletedCount > 0
    );
  }

  /** Update DAPS entry */
  public async updateDapsEntry (
    dapsid: string,
    url: string | undefined = undefined,
    trusted = true
  ): Promise<FederatedDAPSEntry> {
    try {
      const dapsEntry = await this.getDapsEntry(dapsid);
      if (url) {
        const jwks = await this.fetchJWKS(url);
        dapsEntry.url = url;
        dapsEntry.jwks = jwks;
      }
      dapsEntry.trusted = trusted;
      await this.collection?.replaceOne({ dapsid: dapsid }, dapsEntry);
      return dapsEntry;
    } catch (err) {
      throw new Error(
        `[FederatedDapsStore] Cannot update DAPS(dapsid:${dapsid})`
      );
    }
  }
}
