'use strict';

import MongoClient, { DeleteResult, Document, InsertOneResult, UpdateResult } from 'mongodb';
import { connectMongoDB } from '../util/mongo';
import { Configuration } from '../util/configuration';
import { User, UserRole } from '../model';
import crypto from 'crypto';
import bcryptjs from 'bcryptjs';

const configuration = Configuration.getInstance();

export class UserStore {
  /** Singleton instance */
  private static instance: UserStore;
  /** MongoDB database name */
  private readonly dbName = 'users';
  /** MongoDB collection */
  private collection: MongoClient.Collection<User>;
  private interval: NodeJS.Timeout;

  static getInstance (): UserStore {
    if (!UserStore.instance) {
      UserStore.instance = new UserStore();
      UserStore.instance.init();
    }
    return UserStore.instance;
  }

  /** Initialize Federated DAPS store */
  async init (): Promise<void> {
    try {
      const client = await connectMongoDB();
      this.collection = client.db(this.dbName).collection('users');
      const count = await this.collection.countDocuments();
      this.collection.createIndex({ username: 1 }, { unique: true });
      if (count === 0) {
        this.collection.insertOne({
          username: 'admin',
          email: 'noreply@dataspac.es',
          name: 'Admin',
          password: configuration.adminPassword,
          role: UserRole.ADMIN,
          verification: {
            verified: true
          }
        });
      }
    } catch (err) {
      console.log(`[UserStore] Error in mongodb connection: ${err}`);
    }
  }

  /** Validate user credentials and return a User object if validated */
  public async login (login: string, password: string): Promise<User | null> {
    const user = await this.collection?.findOne({
      $or: [
        {
          username: login
        },
        {
          email: login
        }
      ]
    });
    if (user && bcryptjs.compareSync(password, user.password)) {
      user.password = null;
      return user;
    }
    return null;
  }

  /** Retrieve a user based on a username */
  public async getUser (username: string): Promise<User | null> {
    const user = await this.collection?.findOne({ username: username });
    if (user) {
      user.password = undefined;
    }
    return user;
  }

  /** Retrieve a list of all users, includes BCrypt passwords */
  public async list (): Promise<User[]> {
    return this.collection?.find({})?.toArray() || [];
  }

  /** Retrieve a list of all users, with removed password */
  public async listApi (): Promise<User[]> {
    const list = await this.list();
    return list.map((user: User) => {
      return {
        username: user.username,
        email: user.email,
        name: user.name,
        password: undefined,
        role: user.role,
        verification: user.verification
      };
    });
  }

  /** Insert a new user in the database */
  public async insert (user: User): Promise<InsertOneResult<User>> {
    return this.collection?.insertOne(user);
  }

  /** Edit the role of a user */
  public async editRole (
    username: string,
    role: UserRole
  ): Promise<UpdateResult | Document> {
    return this.collection?.updateOne(
      { username: username },
      {
        $set: {
          role: role
        }
      }
    );
  }

  /** Delete a user */
  public async delete (username: string): Promise<DeleteResult> {
    return this.collection?.deleteOne({ username: username });
  }

  /** Verify the users email address based on the username and code that are sent to the address */
  public async verify (username: string, code: string): Promise<boolean> {
    const user = await this.collection?.findOne({ username: username });
    if (
      user &&
      user.verification.code === code &&
      user.verification.expiration > new Date()
    ) {
      this.collection.updateOne(
        { username: username },
        {
          $set: {
            verification: {
              verified: true
            }
          }
        }
      );
      return true;
    } else {
      return false;
    }
  }

  /** Reset the validation code */
  public async resetCode (username: string): Promise<string | null> {
    const user = await this.collection?.findOne({ username: username });
    if (user.verification.verified === false) {
      const code = crypto.randomBytes(20).toString('hex');
      this.collection?.updateOne(
        { username: username },
        {
          $set: {
            verification: {
              verified: false,
              code: code,
              expiration: new Date(new Date().getTime() + 60000)
            }
          }
        }
      );
      return code;
    } else {
      return null;
    }
  }

  /** Set a code for resetting a users password */
  public async forgotPassword (
    username: string
  ): Promise<{ code: string; user: User } | undefined> {
    const user = await this.collection?.findOne({ username: username });
    if (user) {
      const code = crypto.randomBytes(32).toString('hex');
      await this.collection?.updateOne(
        { username: username },
        {
          $set: {
            'verification.code': code,
            'verification.expiration': new Date(new Date().getTime() + 60000)
          }
        }
      );
      return {
        code,
        user
      };
    }
    return null;
  }

  /** Reset a users password, with the currentPassword being either the actual current password or the code that the user received via email */
  public async resetPassword (
    username: string,
    currentPassword: string,
    newPassword: string
  ): Promise<boolean> {
    const user = await this.collection?.findOne({ username: username });
    if (
      user &&
      ((user.verification.code === currentPassword &&
        user.verification.expiration > new Date()) ||
        bcryptjs.compareSync(currentPassword, user.password))
    ) {
      await this.collection?.updateOne(
        { username: username },
        {
          $set: {
            password: bcryptjs.hashSync(newPassword, 10)
          },
          $unset: {
            'verification.code': '',
            'verification.expiration': ''
          }
        }
      );
      return true;
    }
    return false;
  }
}
