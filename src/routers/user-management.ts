import express, { RequestHandler, Router } from 'express';
import { UserStore } from '../stores/user-store';
import { User, UserRole } from '../model';
import passport from 'passport';

export const router: Router = express.Router();

const userStore = UserStore.getInstance();

/** Request handler ensuring a user is logged in */
const ensureAuthenticated: RequestHandler = passport.authenticate(
  'jwt',
  { session: false }
);

/** Request handler ensuring the current user has the ADMIN role */
const ensureAdmin: RequestHandler = (req, res, next) => {
  const user = req.user as User;
  if (user.role !== UserRole.ADMIN) {
    return res.status(403).send('');
  }
  next();
};

/** Retrieve a list of all users */
router.get('/users', ensureAuthenticated, ensureAdmin, async (req, res) => {
  const users = await userStore.listApi();
  res.send(users);
});

/** Delete a user, based on a query parameter */
router.delete('/users', ensureAuthenticated, ensureAdmin, async (req, res) => {
  const username = req.query.username as string;
  try {
    await userStore.delete(username);
    res.send('');
  } catch (e) {
    res.status(500).send(e);
  }
});

/** Update the role of a user */
router.put(
  '/users/updateRole',
  ensureAuthenticated,
  ensureAdmin,
  async (req, res) => {
    const user = req.user as User;
    const username = req.query.username as string;
    if (user.username === username) {
      return res.status(400).json('Can\'t edit your own role');
    }
    const role = UserRole[req.query.role as keyof typeof UserRole];
    try {
      await userStore.editRole(username, role);
      res.send('');
    } catch (e) {
      res.status(500).send(e);
    }
  }
);
