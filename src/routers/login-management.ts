import express, { Router } from 'express';
import bcryptjs from 'bcryptjs';
import crypto from 'crypto';
import { Configuration } from '../util/configuration';
import { UserStore } from '../stores/user-store';
import * as jwt from 'jsonwebtoken';
import { TemplateParameters, User, UserRole } from '../model';
import { sendMail } from '../util/email';
import { jwtSecret } from '../controllers/management';

export const router: Router = express.Router();

const configuration = Configuration.getInstance();
const userStore = UserStore.getInstance();

/** Handle registration request of a new user, with optionally email validation */
router.post('/register', async (req, res) => {
  const userReq = req.body;
  if (!userReq.email || !userReq.password || !userReq.name) {
    return res.status(400).json({ reason: 'Missing fields' });
  }
  const user: User = {
    username: userReq.email,
    email: userReq.email,
    name: userReq.name,
    password: bcryptjs.hashSync(userReq.password, 10),
    role: UserRole.USER,
    verification: {
      verified: false,
      code: crypto.randomBytes(32).toString('hex'),
      expiration: new Date(new Date().getTime() + 60 * 60 * 1000)
    }
  };
  if (configuration.emailValidation) {
    const emailParameters: TemplateParameters = {
      email: userReq.email,
      sender: `"${configuration.title}" <noreply@dataspac.es>`,
      title: `${configuration.dataspace} - Activate your account`,
      summary: `Activate your account for the ${configuration.title}`,
      link: configuration.issuer,
      img: `${configuration.logo}`,
      header: 'Activate your account',
      content: [
        {
          paragraphs: [
            `An account has been created on the ${configuration.title}`,
            'We need to validate your email address to activate your account. Click the following button to activate your account:'
          ]
        },
        {
          button: {
            url: `${configuration.issuer}/management/verify?user=${user.username}&code=${user.verification.code}`,
            text: 'Activate my account'
          }
        }
      ],
      footer: `This email was sent to you by ${configuration.title} because you signed up for an account. If you do not recognise this, you can safely ignore this email.`
    };
    sendMail(emailParameters);
  } else {
    user.verification = {
      verified: true
    };
  }

  await userStore.insert(user);
  res.send('');
});

/** Handle the request for the question to reset the password */
router.post('/forgotpassword', async (req, res) => {
  const email = req.body.email;
  const result = await userStore.forgotPassword(email);
  if (configuration.emailValidation && result) {
    const emailParameters: TemplateParameters = {
      email: email,
      sender: `"${configuration.title}" <noreply@dataspac.es>`,
      title: `${configuration.dataspace} - Reset password`,
      summary: `Reset your password for the ${configuration.title}`,
      link: configuration.issuer,
      img: `${configuration.logo}`,
      header: 'Reset password',
      content: [
        {
          paragraphs: [
            `A request has been made to reset the password for your account on the ${configuration.title}`,
            'Click the following button to reset your password:'
          ]
        },
        {
          button: {
            url: `${configuration.issuer}/?forgot=${result.code
              }&user=${encodeURIComponent(result.user.username)}#management`,
            text: 'Reset password'
          }
        }
      ],
      footer: `This email was sent to you by ${configuration.title} because you requested a reset of your password. If you do not recognise this, you can safely ignore this email.`
    };
    sendMail(emailParameters);
    res.send('');
  } else {
    res
      .status(501)
      .send('Email validation is turned off, no email will be sent.');
  }
});

/** Handle the request for resetting the password, either with the current password of the user or with a code that is received via email */
router.post('/resetpassword', async (req, res) => {
  const email = req.body.email;
  const currentPassword = req.body.currentPassword;
  const newPassword = req.body.newPassword;
  const result = await userStore.resetPassword(
    email,
    currentPassword,
    newPassword
  );
  if (result) {
    res.send('');
  } else {
    res.status(400).send('');
  }
});

/** Handle the verification of the email address, based on a code that has been sent via email */
router.get('/verify', async (req, res) => {
  const code = req.query.code as string;
  const user = req.query.user as string;
  const verified = await userStore.verify(user, code);
  console.log(`Verification: ${user}, ${code}, ${verified}`);
  res.redirect('/?verified=1#management');
});

/** Manually verify the user, in case the email could not be sent. */
router.put('/verifyuser', async (req, res) => {
  const code = req.body.code;
  const user = req.body.user;
  const verified = await userStore.verify(user, code);
  console.log(`Verification: ${user}, ${code}, ${verified}`);
  res.redirect(303, '/?verified=1#management');
});

/** Handle the login of the user and provide a token that can be used for further communication */
router.post('/login', async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const user = await userStore.login(username, password);
  if (user) {
    const token = jwt.sign(
      {
        role: user.role
      },
      jwtSecret,
      {
        issuer: configuration.issuer,
        subject: user.username,
        audience: configuration.issuer,
        expiresIn: '24h'
      }
    );
    return res.json({ user, token });
  } else {
    return res.status(400).json({
      message: 'Incorrect credentials'
    });
  }
});
