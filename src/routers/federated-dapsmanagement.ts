import express, { RequestHandler, Router } from 'express';
import { Configuration } from '../util/configuration';
import { FederatedDapsStore } from '../stores/federated-daps-store';
import passport from 'passport';
import { User, UserRole } from '../model';

export const router: Router = express.Router();

const configuration = Configuration.getInstance();
const federatedDapsStore = FederatedDapsStore.getInstance();

/** Request handler ensuring a user is logged in */
const ensureAuthenticated: RequestHandler = passport.authenticate(
  'jwt',
  { session: false }
);

/** Request handler ensuring the current user has the ADMIN role */
const ensureAdmin: RequestHandler = (req, res, next) => {
  const user = req.user as User;
  if (user.role !== UserRole.ADMIN) {
    return res.status(403).send('');
  }
  next();
};

/** DAPS Federation management */
if (configuration.dapsFederation) {
  /** Retrieve federated DAPS entries  */
  router.get(
    '/federation',
    ensureAuthenticated,
    ensureAdmin,
    async (req, res) => {
      res.send(await federatedDapsStore.getAllDapsEntries());
    }
  );

  /** Add trusted federated DAPS entry */
  router.post(
    '/federation',
    ensureAuthenticated,
    ensureAdmin,
    async (req, res) => {
      try {
        const daps = await federatedDapsStore.addDapsEntry(
          req.body.dapsid,
          req.body.url
        );
        res.send(daps);
      } catch (err) {
        res.status(500).send({ error: err });
      }
    }
  );

  /** Delete federated DAPS entry */
  router.delete(
    '/federation',
    ensureAuthenticated,
    ensureAdmin,
    async (req, res) => {
      try {
        await federatedDapsStore.deleteDapsEntry(req.query.id as string);
        res.send();
      } catch (err) {
        res.status(500).send({ error: err });
      }
    }
  );

  /** Update federated DAPS entry */
  router.put(
    '/federation',
    ensureAuthenticated,
    ensureAdmin,
    async (req, res) => {
      try {
        const daps = await federatedDapsStore.updateDapsEntry(
          req.query.id as string,
          req.body.url,
          req.body.trust
        );
        res.send(daps);
      } catch (err) {
        res.status(500).send({ error: err });
      }
    }
  );
}
