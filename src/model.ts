'use strict';

import { JSONWebKeySet } from 'jose';
import { Bytes, pki } from 'node-forge';

/** Participant certification status */
export interface ParticipantCertification {
  active: boolean;
  lastActive?: Date;
  level?: string;
}

/** Component certification status */
export interface ComponentCertification {
  active: boolean;
  lastActive?: Date;
  securityProfile?: string;
}

/** Participant (public) information, for API use */
export interface Participant {
  id: string;
  contact: string;
  certificate: string;
  certification: ParticipantCertification;
  owner: string;
}

/** Participant (internal) information, for administration use  */
export interface ParticipantInternal extends Participant {
  email: string;
}

/** JWT additional claims */
export interface Claim {
  componentCertification: ComponentCertification;
  participantCertification: ParticipantCertification;
}

/** Component (public) information, for API use */
export interface Component {
  subject?: string; // SKI:keyid:AKI
  name: string;
  idsid?: string;
  participant: string;
  contact: string;
  certificate: string;
  claim: Claim;
}

/** Component (internal) information, for administration use  */
export interface ComponentInternal extends Component {
  email: string;
  owner: string;
}

/** Certification types */
export enum CertificateType {
  PARTICIPANT,
  COMPONENT,
}

/** Certificate Authority class with from PEM to node-forge objects */
export class CertificateAuthority {
  public key: pki.PrivateKey;
  public certificate: pki.Certificate;

  constructor (key: string, certificate: string) {
    this.key = pki.privateKeyFromPem(key);
    this.certificate = pki.certificateFromPem(certificate);
  }

  /** Fingerprint generator for signing Certificate Signing Requests with Authority Key Identifier  */
  public getFingerprint (): Bytes {
    return pki
      .getPublicKeyFingerprint(this.certificate.publicKey, {
        type: 'RSAPublicKey'
      })
      .getBytes();
  }
}

/** Certificate authorities used for signing Certificate Signing Requests */
export interface CertificateAuthorities {
  device: CertificateAuthority;
  participant: CertificateAuthority;
}

/** Federated DAPS entry */
export interface FederatedDAPSEntry {
  dapsid: string;
  url: string;
  jwks: JSONWebKeySet;
  trusted: boolean;
}

/** Enum indicating the role of a user */
export enum UserRole {
  USER = 'USER',
  ADMIN = 'ADMIN',
}

/** Verification information */
export interface Verification {
  verified: boolean;
  code?: string;
  expiration?: Date;
}

/** User information */
export interface User {
  username: string;
  email: string;
  name: string;
  password: string;
  role: UserRole;
  verification: Verification;
}

/** Email content part, either paragraph(s) or a button */
export interface Content {
  paragraphs?: string[];
  button?: { url: string; text: string };
}

/** Email template parameters */
export interface TemplateParameters {
  email: string;
  sender: string;
  title: string;
  summary: string;
  link: string;
  img: string;
  header: string;
  content: Content[];
  footer: string;
}

/** Dynamic Claims Request */
export interface DynamicClaimsRequest {
  [scope: string]: {
    [claim: string]: {
      value: any
    }
  }
}
